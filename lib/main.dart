import 'dart:async';

import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  void initState() {
    super.initState();
    redController = StreamController.broadcast();
    greenController = StreamController.broadcast();
    blueController = StreamController.broadcast();
    greenSubscription = greenController.stream.listen((event) => (green = event));
    redSubscription = redController.stream.listen((event) => (red = event));
    blueSubscription = blueController.stream.listen((event) => (blue = event));
  }

  @override
  void dispose() {
    redSubscription.cancel();
    greenSubscription.cancel();
    blueSubscription.cancel();
    redController.close();
    greenController.close();
    blueController.close();
    super.dispose();
  }

  StreamController<double> redController;
  StreamController<double> greenController;
  StreamController<double> blueController;
  StreamSubscription redSubscription;
  StreamSubscription greenSubscription;
  StreamSubscription blueSubscription;

  double green = 0;
  double red = 0;
  double blue = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          StreamBuilder<double>(
            stream: redController.stream,
            initialData: red,
            builder: (context, snapshot) {
              return Slider(
                value: red,
                max: 255,
                min: 0,
                onChanged: (value) {
                  setState(() {
                    redController.add(red = value);
                  });
                },
              );
            },
          ),
          StreamBuilder<double>(
              stream: greenController.stream,
              initialData: green,
              builder: (context, snapshot) {
                return Slider(
                  value: green.toDouble(),
                  max: 255,
                  min: 0,
                  onChanged: (value) {
                    setState(() {
                      greenController.add(green = value);
                    });
                  },
                );
              }),
          StreamBuilder<double>(
              stream: blueController.stream,
              initialData: blue,
              builder: (context, snapshot) {
                return Slider(
                  value: blue.toDouble(),
                  max: 255,
                  min: 0,
                  onChanged: (value) {
                    setState(() {
                      blueController.add(blue = value);
                    });
                  },
                );
              }),
        ],
      ),

      backgroundColor: Color.fromRGBO(red.toInt(), green.toInt(), blue.toInt(), 1.0),
    );
  }
}
